var express = require('express');
var app = express();
var mongoose = require('mongoose');
const path =require('path');
//connec database mlab
mongoose.connect('mongodb://nodejs:nodejs@ds137650.mlab.com:37650/customer');
const customer = require('./public/models/customer');

mongoose.connection.on('connected', () => {console.log('db success')});
mongoose.connection.once('open', function () {
    // we're connected!
});
var bodyParser = require('body-parser');

app.use(express.static(path.join(__dirname + "/public")));
app.use(bodyParser.json());
app.get("/", (req, res)=>{
    res.sendFile(path.join(__dirname, './public/index.html'));
})
app.get('/customer', function (req, res) {
    console.log('get requeses');
    customer.find()
        .exec()
        .then(docs => {
            console.log("fff");
            console.log(docs);
            res.json({
                data: docs.map(doc => {
                    return {
                        username: doc.username,
                        password: doc.password,
                        fullname: doc.fullname,
                        email: doc.email,
                        title: doc.title,
                        depict: doc.depict
                    }
                })
            })
        })
});

app.listen(process.env.PORT || 3000);
console.log('loading server');